<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteRequest;
use App\Models\Note;
use Illuminate\Http\Request;

class SubNoteController extends Controller
{
    /**
     * @param NoteRequest $request
     * @param $noteId
     * @return mixed
     */
    public function store(NoteRequest $request, $noteId)
    {
        $user = $request->user();
        $user->notes()->findOrFailNote($noteId);
        $subNote = new Note($request->all());
        $subNote->parent_id = intval($noteId);
        return $user->notes()->save($subNote);
    }

    /**
     * @param NoteRequest $request
     * @param $noteId
     * @param $subNoteId
     * @return mixed
     */
    public function update(NoteRequest $request, $noteId, $subNoteId)
    {
        $user = $request->user();
        $subNote = $user->notes()->findOrFailSubNOte($noteId, $subNoteId);
        $subNote->update($request->all());
        return $subNote;
    }

    /**
     * @param Request $request
     * @param $noteId
     * @param $subNoteId
     * @return mixed
     */
    public function destroy(Request $request, $noteId, $subNoteId)
    {
        $user = $request->user();
        $subNote = $user->notes()->findOrFailSubNOte($noteId, $subNoteId);
        return $subNote->delete();
    }
}
